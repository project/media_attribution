# Installation

Use composer to download the module,
When you install the module, it creates the following configurations:

  * Taxonomy vocabulary
    * Licences (media_attribution_licenses) - Includes License Link and License Icon fields
      * The vocabulary is populated with a set of Creative Commons licenses.
  * Paragraphs type
    * License Attribution (license_attribution)
      * This field is automatically added to the Image media type on install.

# Useage

The installation script automatically adds the attribution paragraphs field
to the Image media type if it exists. Otherwise adding attribution
can be done at Admin -> Structure -> Media Types.

In CKEditor, when you click the Embed Media button, the attribution links
fields will show up in the Media along with Title, Alt text, etc.
Alternatively you can edit the media content entity directly.

The attribution paragraph field has the following components

  * Source Work link, including a title and URL. This title is
    usually the title of the original work.

  * Original Author link, including name and URL of the author's home page.

  * License type - optional, the license under which the linked work is released.

  * Free-form attribution text - optional, arbitrary text to clarify attribution. If no
    license is selected, this text can be used to identify copyrighted works with e.g.,
    "All rights reserved, used with permisssion."

This source and author info will be rendered below an embedded media object
when showing the node, following the format preferred by Creative Commons,
outlined at Best Practices for Attribution:

https://wiki.creativecommons.org/wiki/best_practices_for_attribution

# Licenses

Installing the module creates License taxonomy term entries for all of the use
variations of the international Creative Commons licenses. These can be
edited, and other licenses can be added as needed.

## Bulk-loading Licenses via Drush

Run `drush help media_attribution:load_licenses` for how to load license data from a YAML file.

# Authors

Alexander O'Neill https://drupal.org/u/alxp

# Sponsoring Organization

[University of Prince Edward Island Robertson Library](https://library.upei.ca/)
